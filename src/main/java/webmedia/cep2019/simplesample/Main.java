package webmedia.cep2019.simplesample;


import com.espertech.esper.client.*;
import org.apache.log4j.varia.NullAppender;
import webmedia.cep2019.simplesample.event.*;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        //Log configuration
        org.apache.log4j.BasicConfigurator.configure(new NullAppender()); //This just remove the Warnings
        //org.apache.log4j.BasicConfigurator.configure(); //This prints the logs on the console

        //The configuration is used to configure the Esper engine before the processing starts
        Configuration configuration = new Configuration();
        //Add a new event type using a java class
        configuration.addEventType("SensorUpdate", SensorUpdate.class);


        EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider(configuration);

        //Used to manage the CEP rules (and other things)
        EPAdministrator epAdm = epService.getEPAdministrator();

        //Used to send new Events (and other things)
        EPRuntime epRuntime = epService.getEPRuntime();

        //Split the SensorUpdate Event Stream by room id
        EPStatement createContext = epAdm.createEPL("create context SegmentedByRoom partition by roomId from SensorUpdate");

        //Select the average values for each room every 3 seconds
        EPStatement avg3secStatement = epAdm.createEPL("context SegmentedByRoom " +
                "select avg(temperature) as avg_temperature, avg(humidity) as avg_humidity, roomId" +
                " from SensorUpdate.std:groupwin(roomId).win:time_batch(3 sec)");
        avg3secStatement.addListener(new PrintUpdateListener("Average Rule"));


        //Select every sensor update with a temperature over 30
        EPStatement over30 = epAdm.createEPL("select * from SensorUpdate(temperature>30)");
        over30.addListener(new PrintUpdateListener("Over30"));


        //Send Random Events
        Random rand = new Random();
        for(int i=0; i<100; i++){
            SensorUpdate sensorUpdate1 = new SensorUpdate(rand.nextInt(70), rand.nextInt(100), 1);
            epRuntime.sendEvent(sensorUpdate1);
            System.out.println("Sending event: "+sensorUpdate1);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SensorUpdate sensorUpdate2 = new SensorUpdate(rand.nextInt(70), rand.nextInt(100), 2);
            epRuntime.sendEvent(sensorUpdate2);
            System.out.println("Sending event: "+sensorUpdate2);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SensorUpdate sensorUpdate3 = new SensorUpdate(rand.nextInt(70), rand.nextInt(100), 3);
            epRuntime.sendEvent(sensorUpdate3);
            System.out.println("Sending event: "+sensorUpdate3);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}


//UpdateListeners receive the results of a rule activation
//Create an update listener that prints the event properties
class PrintUpdateListener implements UpdateListener{
    String ruleName;

    PrintUpdateListener(String ruleName){
        this.ruleName = ruleName;
    }

    public void update(EventBean[] newData, EventBean[] oldData) {

        System.out.println("\n########------------------- Rule "+ruleName+" Activated -------------------########");
        for (int i = 0; i < newData.length; i++) {
            EventBean event = newData[i];
            //Print the name of the event type (e.g.: SensorUpdate)
            System.out.print("{SensorUpdate: ");

            //Get the list of event properties
            String[] propertyNames = event.getEventType().getPropertyNames();

            //Print the properties and respective values
            for (String propertyName : propertyNames){
                System.out.print(propertyName + "=" + event.get(propertyName) + ", ");
            }
            System.out.println("}");
        }
        System.out.println("########------------------- Rule "+ruleName+" Activated -------------------########\n\n");
    }
}
